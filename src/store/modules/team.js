import axios from "axios";
import Vue from "vue";

const state = () => ({
  name: "Beşiktaş JK",
  allPlayers: [],
});
const getters = {
  lineupPlayers: (state) => state.allPlayers.filter((_p) => _p.pick),
  unPickedPlayers: (state) => state.allPlayers.filter((_p) => !_p.pick),
  unPickedPlayers: (state) => state.allPlayers.filter((_p) => !_p.pick),
  inPlayers: (state) => state.allPlayers.filter((_p) => _p.substituteInMinute),
  outPlayers: (state) =>
    state.allPlayers.filter((_p) => _p.substituteOutMinute),
  completeStatus: (state, getters) => getters.lineupPlayers.length === 11,
};
const mutations = {
  SET_ALL_PLAYERS(state, allPlayers) {
    state.allPlayers = allPlayers;
  },
  TOGGLE_PLAYER_PICK_STATUS(state, playerId) {
    const playerIndex = state.allPlayers.findIndex((_p) => _p.id === playerId);
    Vue.set(state.allPlayers, playerIndex, {
      ...state.allPlayers[playerIndex],
      pick: !state.allPlayers[playerIndex].pick,
    });
  },
  ADD_SUBSTITUTE_PLAYER(
    state,
    { playerId, substituteInMinute, substituteOutMinute }
  ) {
    const playerIndex = state.allPlayers.findIndex((_p) => _p.id === playerId);
    Vue.set(state.allPlayers, playerIndex, {
      ...state.allPlayers[playerIndex],
      substituteInMinute,
      substituteOutMinute,
    });
  },
  CLEAR_ALL_SUBSTITUTE(state) {
    state.allPlayers = state.allPlayers.map((_p) => {
      return {
        ..._p,
        substituteInMinute: null,
        substituteOutMinute: null,
      };
    });
  },
};
const actions = {
  fetchTeam({ commit }) {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await axios.get(
          `${process.env.VUE_APP_API_URL}clubs/${process.env.VUE_APP_CLUB_ID}/players?count=100`
        );
        commit(
          "SET_ALL_PLAYERS",
          data.players.map((_p) => {
            return {
              display_name: _p.display_name,
              id: _p.id,
              position: _p.position,
              image_url: _p.image_url,
              pick: false,
              substituteInMinute: null,
              substituteOutMinute: null,
            };
          })
        );
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },

  togglePick({ commit, getters }, playerId) {
    if (getters.completeStatus) {
      commit("CLEAR_ALL_SUBSTITUTE");
    }
    commit("TOGGLE_PLAYER_PICK_STATUS", playerId);
  },

  addSubstitute({ commit }, { outPlayer, inPlayer, substitutionMinute }) {
    commit("ADD_SUBSTITUTE_PLAYER", {
      playerId: outPlayer.id,
      substituteInMinute: outPlayer.substituteInMinute,
      substituteOutMinute: substitutionMinute,
    });
    commit("ADD_SUBSTITUTE_PLAYER", {
      playerId: inPlayer.id,
      substituteInMinute: substitutionMinute,
      substituteOutMinute: null,
    });
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
