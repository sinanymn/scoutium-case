import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./plugins/axios";
import "./plugins/vue-scroll";
import "./plugins/vue-js-modal";
import "./plugins/vue-select";
import "./plugins/v-mask";
import "./plugins/vee-validate";

import { i18n } from "./plugins/i18n";

import "./assets/scss/main.scss";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
  i18n,
}).$mount("#app");
