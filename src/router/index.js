import Vue from "vue";
import VueRouter from "vue-router";
import Team from "../views/Team.vue";
import Confirmation from "../views/Confirmation.vue";

import store from "../store/index";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Team",
    component: Team,
  },
  {
    path: "/confirmation",
    name: "Confirmation",
    component: Confirmation,
    beforeEnter: (to, from, next) => {
      if (store.getters["team/completeStatus"]) {
        next();
        return;
      }
      next("/");
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
